//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#ifndef vtk_aeva_ext_vtkImageNarrowBand_h
#define vtk_aeva_ext_vtkImageNarrowBand_h

#include "vtk/aeva/ext/AEVAExtModule.h"
#include "vtkImageAlgorithm.h"
#include "vtkSmartPointer.h"

// This class produces a DT or a binary band for poly data or unstructured grids
// In the case of 3d cells, it will produce 0 when inside, distance (or binary)
// when outside
class AEVAEXT_EXPORT vtkImageNarrowBand : public vtkImageAlgorithm
{
public:
  static vtkImageNarrowBand* New();
  vtkTypeMacro(vtkImageNarrowBand, vtkImageAlgorithm);
  void PrintSelf(std::ostream& os, vtkIndent indent) override;

  // Input port 1, this images information is used to produce the output
  void SetReferenceImage(const vtkSmartPointer<vtkImageData>& refImage);

  // If on, produces binary results instead of DT
  vtkSetMacro(UseBinary, bool);
  vtkGetMacro(UseBinary, bool);

  vtkSetMacro(BandWidth, double);
  vtkGetMacro(BandWidth, double);

protected:
  vtkImageNarrowBand();
  ~vtkImageNarrowBand() override = default;

  int FillInputPortInformation(int port, vtkInformation* info) override;
  int RequestData(vtkInformation* request,
    vtkInformationVector** inputVec,
    vtkInformationVector* outputVec) override;

  bool UseBinary = false;
  double BandWidth = 1.0;
};

#endif
