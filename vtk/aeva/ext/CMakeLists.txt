set(classes
  vtkAppendFilterIntIds
  vtkDistanceToFeature
  vtkGlobalIdBooleans
  vtkGrowCharts
  vtkImageNarrowBand
  vtkLSCMFilter
  vtkMedHelper
  vtkMedReader
  vtkMedWriter
  vtkNewFeatureEdges
  vtkProportionalEditElements
  vtkProportionalEditFilter
  vtkProportionalEditRepresentation
  vtkProportionalEditWidget
  vtkSideSetsToScalars
  vtkStarIterator
  vtkTexturePackingFilter
)

set(headers
  vtkVisitation.h
)

if (COMMAND _vtk_module_optional_dependency_exists)
  _vtk_module_optional_dependency_exists(ParaView::RemotingViews
    SATISFIED_VAR test_has_paraview_remotingviews)
else ()
  if (TARGET ParaView::RemotingViews AND ParaView_RemotingViews_FOUND)
    set(test_has_paraview_remotingviews 1)
  else ()
    set(test_has_paraview_remotingviews 0)
  endif ()
endif ()
if(test_has_paraview_remotingviews)
  list(APPEND classes
    vtkPVImageSliceMapper2
    vtkVolumeInspectionRepresentation
    vtkVolumeInspectionWidget
  )
endif()

vtk_module_add_module(VTK::AEVAExt
  CLASSES ${classes}
  HEADERS ${headers}
  HEADERS_SUBDIR "vtk/aeva/ext")

target_include_directories(AEVAExt
  PUBLIC
    $<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}>
    $<BUILD_INTERFACE:${PROJECT_BINARY_DIR}>
    $<INSTALL_INTERFACE:${CMAKE_INSTALL_INCLUDEDIR}/${PROJECT_NAME}/${PROJECT_VERSION}>
)

if (AEVA_ENABLE_TESTING)
  add_subdirectory(Testing)
endif()
