set(test_exclusions
  # terminate called after throwing an instance of 'std::system_error'
  # what():  Resource deadlock avoided
  # https://gitlab.kitware.com/aeva/session/-/issues/6
  "^TestGrowSelection$")

string(REPLACE ";" "|" test_exclusions "${test_exclusions}")
if (test_exclusions)
  set(test_exclusions "(${test_exclusions})")
endif ()
