cmake_minimum_required(VERSION 3.12)

# Tarballs are uploaded by CI to the "ci/smtk" folder, In order to use a new superbuild, please move
# the item from the date stamped directory into the `keep` directory. This makes them available for
# download without authentication and as an indicator that the file was used at some point in CI
# itself.

set(data_host "https://data.kitware.com")

# Determine the tarball to download.
# 20250105 - Update to use Xcode 16.1
if ("$ENV{CMAKE_CONFIGURATION}" MATCHES "vs2022")
  set(file_id "1dEnjJ7pLME_5Axwr5e_0foIQYCCN9h3t")
  set(file_hash "1705ad202d391498f9473095fc677b0fadc571c1433fedf2d3d1e6e956144ac7a466b0dfff21f3113b9cba377a587f208ae3637d7c595b438992deb791b45303")
elseif ("$ENV{CMAKE_CONFIGURATION}" MATCHES "macos")
  set(file_id "1BsGbH_NvSL9T2QQ51vo8JLCBJ4EDeaNw")
  set(file_hash "fd2015d1a9e7e7d913c72f5a782ff9b0df4057a08eb3a756369acbaa2797d3962ee4d2fdf8197326b5190721749de62a6fb0b1d42ba40a77d6054dcfe48ca959")
else ()
  message(FATAL_ERROR
    "Unknown build to use for the superbuild")
endif ()

# Ensure we have a hash to verify.
if (NOT DEFINED file_id OR NOT DEFINED file_hash)
  message(FATAL_ERROR
    "Unknown file and hash for the superbuild")
endif ()

# Download the file.
file(DOWNLOAD
  "https://drive.usercontent.google.com/download?export=download&id=${file_id}&export=download&authuser=0&confirm=t"
  ".gitlab/superbuild.tar.gz"
  STATUS download_status
  EXPECTED_HASH "SHA512=${file_hash}")

# Check the download status.
list(GET download_status 0 res)
if (res)
  list(GET download_status 1 err)
  message(FATAL_ERROR
    "Failed to download superbuild.tar.gz: ${err}")
endif ()

# Extract the file.
execute_process(
  COMMAND
    "${CMAKE_COMMAND}"
    -E tar
    xf ".gitlab/superbuild.tar.gz"
  RESULT_VARIABLE res
  ERROR_VARIABLE err
  ERROR_STRIP_TRAILING_WHITESPACE)
if (res)
  message(FATAL_ERROR
    "Failed to extract superbuild.tar.gz: ${err}")
endif ()
