# We require aeva's version information to be included the CMake Project
# declaration in order to properly include aeva versions when using aeva as an
# SDK. We therefore do not have access to the variable ${PROJECT_SOURCE_DIR}
# at the time aevaVersion.cmake is read. We therefore use
# ${CMAKE_CURRENT_SOURCE_DIR}, since aevaVersion.cmake is called at the top
# level of aeva's build. We also guard against subsequent calls to
# aevaVersion.cmake elsewhere in the build setup where
# ${CMAKE_CURRENT_SOURCE_DIR} may no longer be set to the top level directory.

if (NOT DEFINED aeva_VERSION)

  file(STRINGS ${CMAKE_CURRENT_SOURCE_DIR}/version.txt version_string )

  string(REGEX MATCH "([0-9]+)\\.([0-9]+)\\.([0-9]+)[-]*(.*)"
    version_matches "${version_string}")

  set(aeva_VERSION_MAJOR ${CMAKE_MATCH_1})
  set(aeva_VERSION_MINOR ${CMAKE_MATCH_2})
  set(aeva_VERSION "${aeva_VERSION_MAJOR}.${aeva_VERSION_MINOR}")
  if (DEFINED CMAKE_MATCH_3)
    set(aeva_VERSION_PATCH ${CMAKE_MATCH_3})
    set(aeva_VERSION "${aeva_VERSION}.${aeva_VERSION_PATCH}")
  else()
    set(aeva_VERSION_PATCH 0)
  endif()
  # To be thorough, we should split the label into "-prerelease+metadata"
  # and, if prerelease is specified, use it in determining precedence
  # according to semantic versioning rules at http://semver.org/ .
  # For now, just make the information available as a label:
  if (DEFINED CMAKE_MATCH_4)
    set(aeva_VERSION_LABEL "${CMAKE_MATCH_4}")
  endif()

  set(aeva_VERSION_MINOR_STRING ${aeva_VERSION_MINOR})
  if (aeva_VERSION_MINOR_STRING STREQUAL "0")
    set(aeva_VERSION_MINOR_INT ${aeva_VERSION_MINOR})
  else ()
    string(REGEX REPLACE "^0" "" aeva_VERSION_MINOR_INT ${aeva_VERSION_MINOR})
  endif ()
endif()
