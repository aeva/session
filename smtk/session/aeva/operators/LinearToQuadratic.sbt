<?xml version="1.0" encoding="utf-8" ?>
<!-- Description of the aeva "promotion from linear to quadratic" Operation -->
<SMTK_AttributeResource Version="3">
  <Definitions>
    <include href="smtk/operation/Operation.xml"/>
    <AttDef Type="linear to quadratic" Label="promote linear to quadratic" BaseType="operation">

      <BriefDescription>Generate a quadratic mesh promoted from a linear mesh.</BriefDescription>
      <AssociationsDef Name="source" NumberOfRequiredValues="1">
        <Accepts><Resource Name="smtk::session::aeva::Resource" Filter="face|volume"/></Accepts>
      </AssociationsDef>
    </AttDef>
    <!-- Result -->
    <include href="smtk/operation/Result.xml"/>
    <AttDef Type="result(linear to quadratic)" BaseType="result">
      <ItemDefinitions>
        <Void Name="allow camera reset" IsEnabledByDefault="true" AdvanceLevel="11"/>
      </ItemDefinitions>
    </AttDef>
  </Definitions>
  <Views>
    <View Type="Operation" Title="promote linear to quadratic" TopLevel="true" UseSelectionManager="true">
      <InstancedAttributes>
        <Att Type="linear to quadratic">
          <ItemViews>
            <View Path="/source" Type="qtReferenceTree">
              <PhraseModel Type="smtk::view::ResourcePhraseModel">
                <SubphraseGenerator Type="smtk::view::SubphraseGenerator"/>
                <Badges>
                  <Badge
                    Type="smtk::extension::qt::MembershipBadge"
                    MembershipCriteria="ComponentsWithGeometry"
                    Filter="face|volume"
                    Default="false"/>
                  <Badge
                    Type="smtk::extension::paraview::appcomponents::VisibilityBadge"
                    Default="false"/>
                </Badges>
              </PhraseModel>
            </View>
          </ItemViews>
        </Att>
      </InstancedAttributes>
    </View>
  </Views>
</SMTK_AttributeResource>
