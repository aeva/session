<?xml version="1.0" encoding="utf-8" ?>
<!-- Description of the aeva "normal feature" Operation -->
<SMTK_AttributeResource Version="3">
  <Definitions>
    <include href="smtk/operation/Operation.xml"/>
    <AttDef Type="points of primitives feature" Label="select nodes" BaseType="operation">

      <BriefDescription>Generate a node-selection feature by choosing all points of selected primitives.</BriefDescription>
      <AssociationsDef Name="source" NumberOfRequiredValues="1" Extensible="true">
        <Accepts><Resource Name="smtk::session::aeva::Resource" Filter="cell"/></Accepts>
      </AssociationsDef>

      <ItemDefinitions>

        <String Name="save result as" Optional="true" IsEnabledByDefault="false" AdvanceLevel="1">
          <BriefDescription>When enabled, the resulting primitives are saved
            as persistent reference geometry. When disabled, as an ephemeral
            selection rather that can be further edited by user interaction.
          </BriefDescription>
          <DetailedDescription>
            By default in the graphical interface, this operation will create
            an ephemeral selection that can be modified with interactive tools
            before being "frozen" into persistent reference geometry.
            Scripts may enable this item to force persistent reference
            geometry to be created immediately rather than requiring a
            "Duplicate" operation to do so.
          </DetailedDescription>
        </String>

      </ItemDefinitions>

    </AttDef>
    <!-- Result -->
    <include href="smtk/operation/Result.xml"/>
    <AttDef Type="result(all primitives feature)" BaseType="result">
      <ItemDefinitions>
        <Void Name="allow camera reset" IsEnabledByDefault="true" AdvanceLevel="11"/>
      </ItemDefinitions>
    </AttDef>
  </Definitions>
  <Views>
    <View Type="Operation" Title="select nodes" TopLevel="true" UseSelectionManager="true">
      <InstancedAttributes>
        <Att Type="points of primitives feature">
          <ItemViews>
            <View Path="/source" Type="qtReferenceTree">
              <PhraseModel Type="smtk::view::ResourcePhraseModel">
                <SubphraseGenerator Type="smtk::view::SubphraseGenerator"/>
                <Badges>
                  <Badge
                    Type="smtk::extension::qt::MembershipBadge"
                    MembershipCriteria="ComponentsWithGeometry"
                    Filter="face"
                    Default="false"/>
                  <Badge
                    Type="smtk::extension::paraview::appcomponents::VisibilityBadge"
                    Default="false"/>
                </Badges>
              </PhraseModel>
            </View>
          </ItemViews>
        </Att>
      </InstancedAttributes>
    </View>
  </Views>
</SMTK_AttributeResource>
