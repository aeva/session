//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef __smtk_session_aeva_Export_h
#define __smtk_session_aeva_Export_h

#include "smtk/session/aeva/Operation.h"

namespace smtk
{
namespace session
{
namespace aeva
{

class SMTKAEVASESSION_EXPORT Export : public Operation
{
public:
  smtkTypeMacro(smtk::session::aeva::Export);
  smtkCreateMacro(Export);
  smtkSharedFromThisMacro(smtk::operation::Operation);
  smtkSuperclassMacro(Operation);

protected:
  Result operateInternal() override;
  virtual Result exportVTKImage();
  virtual Result exportMedMesh();

  const char* xmlDescription() const override;
  Result m_result;
};

SMTKAEVASESSION_EXPORT bool exportResource(const smtk::resource::ResourcePtr& resource);

}
}
}

#endif
