<?xml version="1.0" encoding="utf-8" ?>
<!-- Description of the aeva "imprint geometry" Operation -->
<SMTK_AttributeResource Version="3">
  <Definitions>
    <include href="smtk/operation/Operation.xml"/>
    <AttDef Type="imprint geometry" Label="imprint mesh onto image" BaseType="operation">
      <BriefDescription>Resample geometry onto an image.</BriefDescription>
      <DetailedDescription>
        Resample geometry onto an image by splatting geometric primitives onto nearby voxels.
        Contact, distance, or a scalar field can be imprinted.
      </DetailedDescription>
      <AssociationsDef Name="source" Label="Sources" NumberOfRequiredValues="2" Extensible="true">
        <Accepts><Resource Name="smtk::session::aeva::Resource" Filter="face|volume"/></Accepts>
      </AssociationsDef>
    <ItemDefinitions>
      <String Name="imprintMethod" Label="imprint method">
        <BriefDescription>Imprint whether a mesh is in contact, the distance from the mesh, or a scalar point field present on the mesh, onto a volume</BriefDescription>
        <ChildrenDefinitions>
          <Double Name="bandwidth" Label="bandwidth">
              <BriefDescription>The distance (in world coordinates) from the surface still considered in contact.</BriefDescription>
              <DefaultValue>2.0</DefaultValue>
          </Double>
          <Double Name="maxInfluence" Label="max influence">
              <BriefDescription>The distance (in world coordinates) from the surface that the scalar field will contribute to the volume's values.</BriefDescription>
              <DefaultValue>2.0</DefaultValue>
          </Double>
          <String Name="scalarName" Label="field name" Optional="true" IsEnabledByDefault="false">
            <BriefDescription>Specify a scalar point field name to use for imprinting. If empty or disabled, use the "active" scalar field.</BriefDescription>
          </String>
        </ChildrenDefinitions>
        <DiscreteInfo DefaultIndex="0">
          <Structure>
            <Value Enum="segmentation of contact">contact</Value>
            <Items><Item>bandwidth</Item></Items>
          </Structure>
          <Structure>
            <Value Enum="distance from mesh">distance</Value>
          </Structure>
          <Structure>
            <Value Enum="scalar data from mesh">scalar</Value>
              <Items>
                <Item>maxInfluence</Item>
                <Item>scalarName</Item>
              </Items>
          </Structure>
        </DiscreteInfo>
      </String>
	  </ItemDefinitions>
    </AttDef>
    <!-- Result -->
    <include href="smtk/operation/Result.xml"/>
    <AttDef Type="result(interpolated geometry)" BaseType="result">
      <ItemDefinitions>
        <Void Name="allow camera reset" IsEnabledByDefault="true" AdvanceLevel="11"/>
      </ItemDefinitions>
    </AttDef>
  </Definitions>
  <Views>
    <View Type="Operation" Title="imprint mesh onto image" TopLevel="true" UseSelectionManager="true">
      <InstancedAttributes>
        <Att Type="imprint geometry">
          <ItemViews>
            <View Path="/source" Type="qtReferenceTree">
              <PhraseModel Type="smtk::view::ResourcePhraseModel">
                <SubphraseGenerator Type="smtk::view::SubphraseGenerator"/>
                <Badges>
                  <Badge
                    Type="smtk::extension::qt::MembershipBadge"
                    MembershipCriteria="ComponentsWithGeometry"
                    Filter="face|volume"
                    Default="false"/>
                  <Badge
                    Type="smtk::extension::paraview::appcomponents::VisibilityBadge"
                    Default="false"/>
                </Badges>
              </PhraseModel>
            </View>
          </ItemViews>
        </Att>
      </InstancedAttributes>
    </View>
  </Views>
</SMTK_AttributeResource>
