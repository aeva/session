<?xml version="1.0" encoding="utf-8" ?>
<!-- Description of the aeva "proximity feature" Operation -->
<SMTK_AttributeResource Version="3">
  <Definitions>
    <include href="smtk/operation/Operation.xml"/>
    <AttDef Type="proximity feature" Label="select by proximity" BaseType="operation">

      <BriefDescription>Generate a surface selection by choosing cells proximal to a second surface.</BriefDescription>
      <DetailedDescription>
        Generate a surface selection by choosing cells within some given distance to a second surface.

        If no cells are within the specified distance, a message is logged to the console with the
        range of distances between the source and target objects.

        You may also enable thresholding that tests whether the direction of a candidate cell's
        normal is opposite the normal of the closest point on the target surface, to within the
        given angular deviation. Note that this option will usually produce poor results if the
        source and target surfaces intersect.
      </DetailedDescription>
      <AssociationsDef Name="source" NumberOfRequiredValues="1">
        <BriefDescription>The input data from which cells will be selected.</BriefDescription>
        <Accepts><Resource Name="smtk::session::aeva::Resource" Filter="face"/></Accepts>
      </AssociationsDef>

      <ItemDefinitions>

        <Component Name="target" NumberOfRequiredValues="1">
          <BriefDescription>The second surface to which the input must be proximal.</BriefDescription>
          <Accepts><Resource Name="smtk::session::aeva::Resource" Filter="face"/></Accepts>
        </Component>

        <Double Name="distance" Label="distance" NumberOfRequiredValues="1">
          <BriefDescription>Cells with a majority of points within this distance will be selected.</BriefDescription>
          <DefaultValue>0.1</DefaultValue>
          <RangeInfo>
            <Min Inclusive="true">0.</Min>
          </RangeInfo>
        </Double>

        <Double Name="angle" Label="angle threshold" NumberOfRequiredValues="1" Units="degrees"
          Optional="true" IsEnabledByDefault="false">
          <BriefDescription>The maximum allowed angle between opposing surface normals.</BriefDescription>
          <DefaultValue>30.</DefaultValue>
          <RangeInfo>
            <Min Inclusive="true">0.</Min>
            <Min Inclusive="false">180.</Min>
          </RangeInfo>
        </Double>

        <String Name="save result as" Optional="true" IsEnabledByDefault="false" AdvanceLevel="1">
          <DefaultValue>selected by proximity</DefaultValue>
          <BriefDescription>When false, the resulting primitives are saved as
            an ephemeral selection rather than as persistent reference geometry.
            When true, the resulting component is assigned the given name.
          </BriefDescription>
          <DetailedDescription>
            By default in the graphical interface, this operation will create
            an ephemeral selection that can be modified with interactive tools
            before being "frozen" into persistent reference geometry.
            Scripts may enable this item to force persistent reference
            geometry to be created immediately and with the given name
            rather than requiring a "Duplicate" operation to do so.
          </DetailedDescription>
        </String>

      </ItemDefinitions>
    </AttDef>
    <!-- Result -->
    <include href="smtk/operation/Result.xml"/>
    <AttDef Type="result(proximity feature)" BaseType="result">
      <ItemDefinitions>
        <Void Name="allow camera reset" IsEnabledByDefault="true" AdvanceLevel="11"/>
      </ItemDefinitions>
    </AttDef>
  </Definitions>
  <Views>
    <View Type="Operation" Title="select by proximity" TopLevel="true" UseSelectionManager="true">
      <InstancedAttributes>
        <Att Type="proximity feature">
          <ItemViews>
            <View Path="/source" Type="qtReferenceTree">
              <PhraseModel Type="smtk::view::ResourcePhraseModel">
                <SubphraseGenerator Type="smtk::view::SubphraseGenerator"/>
                <Badges>
                  <Badge
                    Type="smtk::extension::qt::MembershipBadge"
                    MembershipCriteria="ComponentsWithGeometry"
                    Filter="face"
                    Default="false"/>
                  <Badge
                    Type="smtk::extension::paraview::appcomponents::VisibilityBadge"
                    Default="false"/>
                </Badges>
              </PhraseModel>
            </View>
          </ItemViews>
        </Att>
      </InstancedAttributes>
    </View>
  </Views>
</SMTK_AttributeResource>
