<?xml version="1.0" encoding="utf-8" ?>
<!-- Description of the AEVA session "volune mesher" Operation -->
<SMTK_AttributeResource Version="3">
  <Definitions>
    <include href="smtk/operation/Operation.xml"/>
    <AttDef Type="volume mesher" Label="mesh volume" BaseType="operation">
      <BriefDescription>Create a volumetric mesh from an input surface mesh with NetGen.</BriefDescription>
      <DetailedDescription>
        Given a closed bounding surface composed of triangles, this operation
        creates a tetrahedral volumetric mesh of the interior using NetGen.
      </DetailedDescription>
      <AssociationsDef Name="source" NumberOfRequiredValues="1">
        <BriefDescription>The input surface to mesh</BriefDescription>
        <Accepts><Resource Name="smtk::session::aeva::Resource" Filter="face"/></Accepts>
      </AssociationsDef>

      <ItemDefinitions>

        <Double Name="max global mesh size" Label="max global cell size">
          <BriefDescription>Maximum global mesh-primitive size allowed</BriefDescription>
          <DefaultValue>1.0e6</DefaultValue>
          <RangeInfo>
            <Min Inclusive="false">0.0</Min>
          </RangeInfo>
        </Double>

        <Double Name="mesh density" Label="mesh density">
          <BriefDescription>Mesh density: 0...1 (0 => coarse; 1 => fine)</BriefDescription>
          <DefaultValue>0.5</DefaultValue>
          <RangeInfo>
            <Min Inclusive="true">0.0</Min>
            <Max Inclusive="true">1.0</Max>
          </RangeInfo>
        </Double>

        <Int Name="element order" Label="output order">
          <BriefDescription>Generate first-order (linear) or second-order (quadratic) elements</BriefDescription>
          <DefaultValue>1</DefaultValue>
          <RangeInfo>
            <Min Inclusive="true">1</Min>
            <Max Inclusive="true">2</Max>
          </RangeInfo>
        </Int>

        <Int Name="optimize step number" Label="optimization iterations">
          <BriefDescription>Number of optimize steps to use for 3-D mesh optimization</BriefDescription>
          <DefaultValue>3</DefaultValue>
          <RangeInfo>
            <Min Inclusive="true">0</Min>
          </RangeInfo>
        </Int>

      </ItemDefinitions>
    </AttDef>
    <!-- Result -->
    <include href="smtk/operation/Result.xml"/>
    <AttDef Type="result(volume mesher)" BaseType="result"/>
  </Definitions>
  <Views>
    <View Type="Operation" Title="mesh volume" TopLevel="true" UseSelectionManager="true">
      <InstancedAttributes>
        <Att Type="volume mesher">
          <ItemViews>
            <View Path="/source" Type="qtReferenceTree">
              <PhraseModel Type="smtk::view::ResourcePhraseModel">
                <SubphraseGenerator Type="smtk::view::SubphraseGenerator"/>
                <Badges>
                  <Badge
                    Type="smtk::extension::qt::MembershipBadge"
                    MembershipCriteria="ComponentsWithGeometry"
                    Filter="face"
                    Default="false"/>
                  <Badge
                    Type="smtk::extension::paraview::appcomponents::VisibilityBadge"
                    Default="false"/>
                </Badges>
              </PhraseModel>
            </View>
          </ItemViews>
        </Att>
      </InstancedAttributes>
    </View>
  </Views>
</SMTK_AttributeResource>
