<?xml version="1.0" encoding="utf-8" ?>
<!-- Description of the aeva "reconstruct surface" Operation -->
<SMTK_AttributeResource Version="3">
  <Definitions>
    <include href="smtk/operation/Operation.xml"/>
    <AttDef Type="reconstruct surface" Label="reconstruct surface" BaseType="operation">

      <BriefDescription>Reconstruct a surface using its points and point normals</BriefDescription>
      <DetailedDescription>
        Reconstruct a surface using its points and point normals.

        Given a surface with point normals, the dimensions of an
        enclosing voxel grid and a radius of influence, apply a
        variant of Curless et al.:

        Brian Curless and Marc Levoy. 1996. A volumetric method for
        building complex models from range images. In Proceedings of
        the 23rd annual conference on Computer graphics and
        interactive techniques (SIGGRAPH ’96). Association for
        Computing Machinery, New York, NY, USA, 303–312.
        DOI:https://doi.org/10.1145/237170.237269

        This method constructs an implicit signed distance function
        from the input dataset's points and point normals, using the
        radius of influence to define a local neighborhood for
        computing the signed distance. It then computes an isocontour
        at distance = 0 using a variant of the Flying Edges technique:

        W. Schroeder, R. Maynard and B. Geveci, "Flying edges: A
        high-performance scalable isocontouring algorithm," 2015 IEEE 5th
        Symposium on Large Data Analysis and Visualization (LDAV), Chicago,
        IL, 2015, pp. 33-40, doi: 10.1109/LDAV.2015.7348069.
      </DetailedDescription>
      <AssociationsDef Name="source" NumberOfRequiredValues="1">
        <BriefDescription>The input surface to reconstruct.</BriefDescription>
        <Accepts><Resource Name="smtk::session::aeva::Resource" Filter="face"/></Accepts>
      </AssociationsDef>

      <ItemDefinitions>

        <Int Name="dimensions" Label="dimensions" NumberOfRequiredValues="3">
          <BriefDescription>Dimensions of the original voxel grid from
          which the surface was generated.</BriefDescription>
          <ComponentLabels>
            <Label>X</Label>
            <Label>Y</Label>
            <Label>Z</Label>
          </ComponentLabels>
          <RangeInfo>
            <Min Inclusive="false">0</Min>
          </RangeInfo>
        </Int>

        <Double Name="radius" Label="radius of influence">
          <BriefDescription>Radius defining a patch size to compute a signed distance field..</BriefDescription>
          <RangeInfo>
            <Min Inclusive="true">0</Min>
          </RangeInfo>
        </Double>

      </ItemDefinitions>
    </AttDef>
    <!-- Result -->
    <include href="smtk/operation/Result.xml"/>
    <AttDef Type="result(reconstruct surface)" BaseType="result"/>
  </Definitions>
  <Views>
    <View Type="Operation" Title="reconstruct surface" TopLevel="true" UseSelectionManager="true">
      <InstancedAttributes>
        <Att Type="reconstruct surface">
          <ItemViews>
            <View Path="/source" Type="qtReferenceTree">
              <PhraseModel Type="smtk::view::ResourcePhraseModel">
                <SubphraseGenerator Type="smtk::view::SubphraseGenerator"/>
                <Badges>
                  <Badge
                    Type="smtk::extension::qt::MembershipBadge"
                    MembershipCriteria="ComponentsWithGeometry"
                    Filter="face"
                    Default="false"/>
                  <Badge
                    Type="smtk::extension::paraview::appcomponents::VisibilityBadge"
                    Default="false"/>
                </Badges>
              </PhraseModel>
            </View>
          </ItemViews>
        </Att>
      </InstancedAttributes>
    </View>
  </Views>
</SMTK_AttributeResource>
