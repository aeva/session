<?xml version="1.0" encoding="utf-8" ?>
<!-- Description of the AEVA session "imprint image" Operation -->
<SMTK_AttributeResource Version="3">
  <Definitions>
    <include href="smtk/operation/Operation.xml"/>
    <AttDef Type="imprint image" Label="imprint image onto mesh" BaseType="operation">

      <BriefDescription>Imprint nodal attributes from an image onto a mesh.</BriefDescription>
      <DetailedDescription>
        Imprint the scalar values from a vtkImageData onto points of the target poly data/unstructured grid.
      </DetailedDescription>
      <AssociationsDef Name="targets" NumberOfRequiredValues="1" Extensible="true">
        <BriefDescription>The input unstructured mesh data. Point field data from the image will be added.</BriefDescription>
        <Accepts><Resource Name="smtk::session::aeva::Resource" Filter="face|volume"/></Accepts>
      </AssociationsDef>

      <ItemDefinitions>
        <Component Name="image" NumberOfRequiredValues="1">
          <BriefDescription>The source image to be probed for scalars.</BriefDescription>
          <Accepts><Resource Name="smtk::session::aeva::Resource" Filter="volume"/></Accepts>
        </Component>
      </ItemDefinitions>

    </AttDef>
    <!-- Result -->
    <include href="smtk/operation/Result.xml"/>
    <AttDef Type="result(imprint image)" BaseType="result"/>
  </Definitions>
  <Views>
    <View Type="Operation" Title="imprint image onto mesh" TopLevel="true" UseSelectionManager="true">
      <InstancedAttributes>
        <Att Type="imprint image">
          <ItemViews>
            <View Path="/targets" Type="qtReferenceTree">
              <PhraseModel Type="smtk::view::ResourcePhraseModel">
                <SubphraseGenerator Type="smtk::view::SubphraseGenerator"/>
                <Badges>
                  <Badge
                    Type="smtk::extension::qt::MembershipBadge"
                    MembershipCriteria="ComponentsWithGeometry"
                    Filter="face|volume"
                    Default="false"/>
                  <Badge
                    Type="smtk::extension::paraview::appcomponents::VisibilityBadge"
                    Default="false"/>
                </Badges>
              </PhraseModel>
            </View>
            <View Path="/image" Type="qtReferenceTree">
              <PhraseModel Type="smtk::view::ResourcePhraseModel">
                <SubphraseGenerator Type="smtk::view::SubphraseGenerator"/>
                <Badges>
                  <Badge
                    Type="smtk::extension::qt::MembershipBadge"
                    MembershipCriteria="ComponentsWithGeometry"
                    Filter="volume"
                    Default="false"/>
                  <Badge
                    Type="smtk::extension::paraview::appcomponents::VisibilityBadge"
                    Default="false"/>
                </Badges>
              </PhraseModel>
            </View>
          </ItemViews>
        </Att>
      </InstancedAttributes>
    </View>
  </Views>
</SMTK_AttributeResource>
