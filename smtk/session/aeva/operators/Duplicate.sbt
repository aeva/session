<?xml version="1.0" encoding="utf-8" ?>
<!-- Description of the aeva "duplicate" Operation -->
<SMTK_AttributeResource Version="3">
  <Definitions>
    <include href="smtk/operation/Operation.xml"/>
    <AttDef Type="duplicate" Label="duplicate" BaseType="operation">

      <BriefDescription>Create new cell(s) that are copies of the selected cell(s).</BriefDescription>
      <AssociationsDef Name="source" NumberOfRequiredValues="1" Extensible="true">
        <Accepts><Resource Name="smtk::session::aeva::Resource" Filter="vertex|face"/></Accepts>
      </AssociationsDef>
    </AttDef>
    <!-- Result -->
    <include href="smtk/operation/Result.xml"/>
    <AttDef Type="result(duplicate)" BaseType="result">
      <ItemDefinitions>
        <Void Name="allow camera reset" IsEnabledByDefault="true" AdvanceLevel="11"/>
      </ItemDefinitions>
    </AttDef>
  </Definitions>
  <Views>
    <View Type="Operation" Title="duplicate" TopLevel="true" UseSelectionManager="true">
      <InstancedAttributes>
        <Att Type="duplicate">
          <ItemViews>
            <View Path="/source" Type="qtReferenceTree">
              <PhraseModel Type="smtk::view::ResourcePhraseModel">
                <SubphraseGenerator Type="smtk::view::SubphraseGenerator"/>
                <Badges>
                  <Badge
                    Type="smtk::extension::qt::MembershipBadge"
                    MembershipCriteria="ComponentsWithGeometry"
                    Filter="vertex|face"
                    Default="false"/>
                  <Badge
                    Type="smtk::extension::paraview::appcomponents::VisibilityBadge"
                    Default="false"/>
                </Badges>
              </PhraseModel>
            </View>
          </ItemViews>
        </Att>
      </InstancedAttributes>
    </View>
  </Views>
</SMTK_AttributeResource>
