//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#ifndef pybind_smtk_session_aeva_Resource_h
#define pybind_smtk_session_aeva_Resource_h

#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

#include "smtk/session/aeva/Resource.h"

namespace py = pybind11;

inline PySharedPtrClass< smtk::session::aeva::Resource, smtk::model::Resource >
pybind11_init_smtk_session_aeva_Resource(py::module &m)
{
  PySharedPtrClass< smtk::session::aeva::Resource, smtk::model::Resource > instance(m, "Resource");
  instance
    .def_readonly_static("type_name", &smtk::session::aeva::Resource::type_name)
    .def_static("create", (std::shared_ptr<smtk::session::aeva::Resource> (*)()) &smtk::session::aeva::Resource::create)
    .def("typeName", &smtk::session::aeva::Resource::typeName)
    .def("session", &smtk::session::aeva::Resource::session)
    .def("setSession", &smtk::session::aeva::Resource::setSession, py::arg("session"))
    ;
  return instance;
}

#endif
