//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#ifndef pybind_smtk_session_aeva_Predicates_h
#define pybind_smtk_session_aeva_Predicates_h

#include <pybind11/pybind11.h>

#include "smtk/session/aeva/Predicates.h"

#include "smtk/extension/vtk/pybind11/PybindVTKTypeCaster.h"

#include "vtkDataObject.h"
#include "vtkPolyData.h"
#include "vtkUnstructuredGrid.h"

namespace py = pybind11;

inline void pybind11_init_smtk_session_aeva_Iterate(py::module &m)
{
  py::enum_<smtk::session::aeva::Iterate>(m, "Iterate")
    .value("Continue", smtk::session::aeva::Iterate::Continue)
    .value("Break", smtk::session::aeva::Iterate::Break)
    .export_values();
}

inline void pybind11_init_smtk_session_aeva_NumberOfCells(py::module &m)
{
  m.def("NumberOfCells", &smtk::session::aeva::NumberOfCells, "", py::arg("data"));
}

inline void pybind11_init_smtk_session_aeva_DimensionFromCellType(py::module &m)
{
  m.def("DimensionFromCellType", &smtk::session::aeva::DimensionFromCellType, "", py::arg("cellType"));
}

inline void pybind11_init_smtk_session_aeva_EstimateParametricDimension(py::module &m)
{
  m.def("EstimateParametricDimension", &smtk::session::aeva::EstimateParametricDimension, "", py::arg("data"));
}

inline void pybind11_init_smtk_session_aeva_EntityType(py::module &m)
{
  m.def("EntityType", &smtk::session::aeva::EntityType, "", py::arg("data"));
}

inline void pybind11_init_smtk_session_aeva_EdgeNeighbors(py::module &m)
{
  m.def("EdgeNeighbors", &smtk::session::aeva::EdgeNeighbors, "", py::arg("data"));
}

inline void pybind11_init_smtk_session_aeva_SurfaceWithNormals(py::module &m)
{
  m.def("SurfaceWithNormals", &smtk::session::aeva::SurfaceWithNormals, "", py::arg("data"), py::arg("surf"), py::arg("norm"));
}

#endif
