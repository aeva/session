//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef smtk_session_aeva_OperationDecorator_h
#define smtk_session_aeva_OperationDecorator_h

#include "smtk/session/aeva/Exports.h"
#include "smtk/view/OperationDecorator.h"

namespace smtk
{
namespace session
{
namespace aeva
{

/**\brief The set of operations to expose in aeva's toolbox.
  *
  */
class SMTKAEVASESSION_EXPORT OperationDecorator : public smtk::view::OperationDecorator
{
public:
  OperationDecorator();
  ~OperationDecorator() override = default;
};

} // namespace aeva
} // namespace session
} // namespace smtk

#endif
