//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef smtk_session_aeva_CompilerInfo_h
#define smtk_session_aeva_CompilerInfo_h

#if defined(_MSC_FULL_VER) && _MSC_FULL_VER < 192930040
#define AEVA_THIRDPARTY_PRE_INCLUDE                                                                \
  __pragma(warning(push)) __pragma(warning(disable : 4348)) /*redefinition of default parameter*/

#define AEVA_THIRDPARTY_POST_INCLUDE __pragma(warning(pop))

#else

#define AEVA_THIRDPARTY_PRE_INCLUDE
#define AEVA_THIRDPARTY_POST_INCLUDE

#endif

#endif // smtk_session_aeva_CompilerInfo_h
